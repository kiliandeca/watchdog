import { EventHandler } from "./EventHandler"
import { Message } from "discord.js"
import { Commands, ButterCommandHandler, PingCommandHandler, HelpCommandHandler, VersionCommandHandler, UninstallCommandHandler, InfoCommandHandler, InstallCommandHandler, AdminCommandHandler, ClearCommandHandler } from "../commands";
import { CommandHandler } from "../commands/CommandHandler";
import { PermissionsService } from "../services";

const CMD_PREFIX = process.env.CMD_PREFIX || '!'

const CommandToHandlerMap = new Map<Commands, CommandHandler> ([
    [Commands.PING, PingCommandHandler],
    [Commands.HELP, HelpCommandHandler],
    [Commands.VERSION, VersionCommandHandler],
    [Commands.INSTALL, InstallCommandHandler],
    [Commands.UNINSTALL, UninstallCommandHandler],
    [Commands.INFO, InfoCommandHandler],
    [Commands.PASS_BUTTER, ButterCommandHandler],
    [Commands.ADMIN, AdminCommandHandler],
    [Commands.CLEAR, ClearCommandHandler]
])

class MessageEventHandler implements EventHandler {

    async handle(msg: Message): Promise<void> {

        if (msg.content.startsWith(CMD_PREFIX) && !msg.author.bot) {
            
            msg.content = msg.content.slice(CMD_PREFIX.length)
            try {
                await this.handleCommand(msg)
            } catch (e){                
                msg.reply(`⚠️ ${e.name}: ${e.message}`)
            }
            
        }

    }

    async handleCommand(message: Message) {
        console.log(message.content);
    

        if (!message.member) {
            return;
        }

        const args = message.content.split(' ');
        const command = args.shift() as Commands || Commands.HELP
    
        const commandHandler = CommandToHandlerMap.get(command)
    
        if (!commandHandler) {
            message.reply('Unknown command see: `!help`');
            return;
        }

        if ( message.guild ) {
            if (! await PermissionsService.userIsAuthorizedForRank(commandHandler.permissionNeeded, message.guild, message.member.id)) {
                message.reply("Sorry not sorry but you can't do that !");
                return;
            }

        }
        

        const result = await commandHandler.handle(message)
    
        if (result) {
            message.reply(result)
        }
        
    }
}

const instance = new MessageEventHandler()
export { instance as MessageEventHandler }
