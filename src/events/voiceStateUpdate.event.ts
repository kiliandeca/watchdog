import { VoiceState } from 'discord.js';

import { DynachanManager } from '../services/dynachan';
import { DynachanAdministration } from '../services/dynachan/dynachanAdministration.service';
import { EventHandler } from './EventHandler';

class VoiceStateUpdateEventHandler implements EventHandler {

    async handle(oldState: VoiceState, newState: VoiceState): Promise<void> {

        const member = newState.member;
        if(!member) {return;}

        const newUserChannel = newState.channel
        const guild = newState.guild
    
        const dynachanId = await DynachanAdministration.getDynachanId(guild.id)        
        
            
        // TODO: ajouter une BD Redis avec info nom chan dynamique et autres
        if( newUserChannel && newUserChannel.id == dynachanId) {
        
            // User Joins a voice channel
            await DynachanManager.asignGroupChannel(guild, member)
    
        
        } else if(newUserChannel === undefined){
        
            // User leaves a voice channel
        
        }

    }
}

const instance = new VoiceStateUpdateEventHandler()
export { instance as VoiceStateUpdateEventHandler }
