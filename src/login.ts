import { Client, Constants } from "discord.js";
import { blue, cyan, red } from "chalk"

const CLIENT_READY = Constants.Events.CLIENT_READY

export function login (client: Client) {
    const token = process.env.BOT_TOKEN
    if (!token){
        console.error(red('Missing Bot Token !'))
        process.exit(1)
    }

    client.on(CLIENT_READY, () => {
        if (!client.user) return;
    
        console.info(`Logged in as ${cyan(client.user.tag)}!`);
        console.info('Server list:')
        client.guilds.cache.forEach(guild => console.info('\t' + blue(guild.name)))
    });

    client.login(token);
}