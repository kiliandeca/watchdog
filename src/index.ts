import { Client, DiscordAPIError} from "discord.js"
import { config } from "dotenv"
import { login } from "./login";
import { Events } from "./events/Events";
import { MessageEventHandler } from "./events/message.event";
import { VoiceStateUpdateEventHandler } from "./events/voiceStateUpdate.event";
import { EventHandler } from "./events/EventHandler";
import {DynachanManager } from './services/dynachan'


// Load .env file
config()


const client = new Client();

login(client)


const EventToHandlerMap = new Map<Events, EventHandler> ([
    [Events.MESSAGE, MessageEventHandler],
    [Events.VOICESTATEUPDATE, VoiceStateUpdateEventHandler]
])

EventToHandlerMap.forEach( (handler, event) => { 
  client.on(event, async (...args) => {
  
    try {
      await handler.handle(...args)
    } catch (e) {
      // TODO: add logs channel
      console.error(e)
    }
  })
})

// Watch for inactive channel every 5 second
setInterval(async () => {
  DynachanManager.checkGroupChannelsInactivity(client)
}, 5000)