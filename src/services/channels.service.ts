import { GuildChannelManager, Channel, CategoryChannel, VoiceChannel, TextChannel } from "discord.js";

enum ChannelType {
    VOICE = 'voice',
    CATEGORY = 'category',
    TEXT = 'text' 
}

async function createChannel(channels: GuildChannelManager, name: string, category: ChannelType, parent?: Channel) {
    return channels.create(name, {type: category, parent: parent})
}

export async function createCategory(channels: GuildChannelManager, name: string): Promise<CategoryChannel>{
    return createChannel(channels, name, ChannelType.CATEGORY) as Promise<CategoryChannel>
}

export async function createVoiceChannel(channels: GuildChannelManager, name: string, parent?: CategoryChannel): Promise<VoiceChannel>{
    return createChannel(channels, name, ChannelType.VOICE, parent) as Promise<VoiceChannel>
}

export async function createTextChannel(channels: GuildChannelManager, name: string, parent?: CategoryChannel): Promise<TextChannel>{
    return createChannel(channels, name, ChannelType.TEXT, parent) as Promise<TextChannel>
}

