import { Guild, GuildMember, Snowflake, GuildChannel, Client, VoiceChannel } from "discord.js";
import { createCategory, createTextChannel, createVoiceChannel } from "..";
import { tedis } from "../redis.service";
import { GroupChannel } from "../../models";

export class DynachanManager {


    static async asignGroupChannel(guild: Guild, member: GuildMember) {

        let voiceChannelId;

        const existingGroupChannel = await this.getGroupChannelOwnedBy(guild.id, member.id)
        if (existingGroupChannel) {
            voiceChannelId = existingGroupChannel.voiceId
        } else {
            const newGroupChannel = await this.createGroupChannel(guild, member)
            voiceChannelId = newGroupChannel.voiceId
        }

        console.log("test");
        

        const voiceChannel = await guild.channels.resolve(voiceChannelId)
        member?.voice.setChannel(voiceChannel)


    }

    static async createGroupChannel(guild: Guild, member: GuildMember) {

        const nextNumber = await this.getNextChannelNumber(guild.id)

        const channelsManager = guild.channels
        const catName = `CHANNEL ${nextNumber}: ${member.displayName}`
        const name = `channel-${nextNumber}`

        const cat = await createCategory(channelsManager, catName)
        
        const result = await Promise.all([
            createTextChannel(channelsManager, name, cat),
            createVoiceChannel(channelsManager, name, cat)
        ]);

        const textChan = result[0];
        const voiceChan = result[1];

        const groupChannel: GroupChannel = {
            guildId: guild.id,
            ownerId: member.id,
            ownerName: member.displayName,
            categoryId: cat.id,
            voiceId: voiceChan.id,
            textId: textChan.id,
            number: nextNumber,
            lastActivity: Date.now()
        }

        await tedis.hmset(`${guild.id}:dynachan:${textChan.id}`, groupChannel as any)

        return groupChannel


        //setTimeout(() => this.deleteGroupChannel(guild, textChan.id), 2000)
    }

    static async getNextChannelNumber(guildId: Snowflake) {

        const groupsChannels = await this.getGroupChannels(guildId)
        const sortedGroupsChannels = groupsChannels.sort((a, b) => a.number - b.number)

        let nextNumber = 1
        for (const groupChan of sortedGroupsChannels) {
            if (groupChan.number != nextNumber) {
                break;
            }
            nextNumber++
        }

        return nextNumber
    }

    static async deleteGroupChannel(guild: Guild, groupChannelId: string) {
        const channelsManager = guild.channels;

        const groupChannel = await this.getGroupChannel(guild.id, groupChannelId);

        const textChannel = channelsManager.resolve(groupChannelId)
        const voiceChannel = channelsManager.resolve(groupChannel.voiceId)
        const catChannel = channelsManager.resolve(groupChannel.categoryId)

        await Promise.all([
            textChannel?.delete(),
            voiceChannel?.delete(),
        ])

        // We do delete the category after to prevent the interface glitch
        await catChannel?.delete()

        await tedis.del(`${guild.id}:dynachan:${groupChannelId}`)

    }

    static async deleteEveryGroupChannel(guild: Guild) {
        const groupChannels = await this.getGroupChannels(guild.id)
        groupChannels.forEach(groupChan => this.deleteGroupChannel(guild, groupChan.textId))
    }


    static async getGroupChannel(guildId: Snowflake, groupChannelId: string) {     
        return await tedis.hgetall(`${guildId}:dynachan:${groupChannelId}`) as unknown as GroupChannel
    }


    static async getGroupChannelOwnedBy(guildId: Snowflake, memberId: Snowflake) {

        const groupChannels = await this.getGroupChannels(guildId)
        const groupChannel = groupChannels.find(groupChannel => groupChannel.ownerId === memberId)

        if (!groupChannel) { return false }

        return groupChannel
        
    }

    static async getGroupChannels(guildId: Snowflake) {
        const groupChannelsKeys = await tedis.keys(`${guildId}:dynachan:???*`)
        return this.getGroupChannelFromKeys(groupChannelsKeys)
    }

    static async getEveryGroupChannels() {
        const groupChannelsKeys = await tedis.keys(`*:dynachan:???*`)
        return this.getGroupChannelFromKeys(groupChannelsKeys)
    }

    static async getGroupChannelFromKeys(keys: string[]){
        const groupChannels = await Promise.all(keys.map(key => tedis.hgetall(key)))
        return groupChannels as unknown as GroupChannel[]
    }

    static async checkGroupChannelsInactivity(client: Client){
        const groupChannels = await this.getEveryGroupChannels();

        groupChannels.forEach(groupChan => {
            const channel = client.channels.resolve(groupChan.voiceId)
            
            if (!channel) {
                // Handle error: maybe delete group chan
                console.log(`Can't resolve channel ${groupChan.voiceId}, maybe someone deleted it ?`)
                return;
            }

            if (channel.type != "voice") {
                // Looks like we fucked up !
                // There is a programming error and the Voice Channel registered in DB is not a Voice Channel
                // I bet this is Franck, please proceed with a git blame
                console.error(`Channel ${groupChan.voiceId} should be a VoiceChannel but it look like it's not`)
                return;
            }

            const voiceChannel = channel as VoiceChannel

            if (voiceChannel.members.size > 0) {
                tedis.hmset(`${voiceChannel.guild.id}:dynachan:${groupChan.textId}`, {lastActivity: Date.now()})
                return;
            }
            
            // If inactive for x secondes we delete            
            if ((Date.now() - groupChan.lastActivity) > 10*1000) {
                // Too old
                this.deleteGroupChannel(voiceChannel.guild, groupChan.textId);
            }

        })

    }

}
