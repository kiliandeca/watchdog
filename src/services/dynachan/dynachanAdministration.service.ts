import { Guild, VoiceChannel } from "discord.js";
import { createCategory, createVoiceChannel } from "../channels.service";
import { tedis } from "../redis.service";
import { DynachanManager } from ".";

const HEAVY_PLUS_SIGN = '➕'

export class DynachanAdministration {

    static async installDynachan(guild: Guild) {

        if (await this.getDynachan(guild)) {
            throw new Error("Dynachan already init")
        }
        
        const cat = await createCategory(guild.channels, "Salons Dynamiques")
        const voice = await createVoiceChannel(guild.channels, `${HEAVY_PLUS_SIGN} Ajouter un channel`, cat)
    
        await tedis.set(`${guild.id}:dynachan:id`, voice.id)
    
    }

    static async uninstallDynachan(guild: Guild) {

        const dynachan = await this.getDynachan(guild)
    
        if (!dynachan) {
            throw new Error("Can't find dynachan");
        }
    
        await dynachan.delete()
        await dynachan.parent?.delete()

        await tedis.del(`${guild.id}:dynachan:id`)
        
        DynachanManager.deleteEveryGroupChannel(guild);
    
    }


    static async  getDynachanId(guildId: string): Promise<string | false>{
        const chanId = await tedis.get(`${guildId}:dynachan:id`)
        if (!chanId) {
            return false;
        }
    
        return chanId.toString();
    }
    
    static async getDynachan(guild: Guild): Promise<VoiceChannel | false>{
        const chanId = await this.getDynachanId(guild.id);
        if (!chanId) {
            return false;
        }
    
        const chan = guild.channels.resolve(chanId)
        if (!chan) {
            return false;
        }
    
        return chan as VoiceChannel;
    }

}




