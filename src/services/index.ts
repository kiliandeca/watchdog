import { createCategory, createVoiceChannel, createTextChannel } from './channels.service'
import { tedis } from './redis.service'
import { Ranks, PermissionsService} from "./permissions.service"

export { createCategory, createVoiceChannel, createTextChannel, tedis, Ranks, PermissionsService }