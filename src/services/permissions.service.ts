import { Guild, Snowflake } from "discord.js"
import { tedis } from "./redis.service"

export enum Ranks {
    USER,
    ADMIN,
    OWNER,
} 

class PermissionsService {

    async getAdminsIds(guild: Guild) {
        const adminsIds = await tedis.smembers(`${guild.id}:permissions:admin`);
        return adminsIds
    }

    async addAdmin(guild: Guild, memberId: Snowflake) {
        await tedis.sadd(`${guild.id}:permissions:admin`, memberId)
    }

    async removeAdmin(guild: Guild, memberId: Snowflake) {
        await tedis.srem(`${guild.id}:permissions:admin`, memberId)
    }

    async userIsAuthorizedForRank(rank: Ranks, guild: Guild, memberId: Snowflake) {

        // Everyone can do USER actions
        if (rank === Ranks.USER) {
            return true;
        }

        const userList = []

        // Here we don't use the break statement between case
        // Owner can do what Admin can do
        switch (rank) {
            case Ranks.ADMIN:
                const adminsIds = await this.getAdminsIds(guild)
                userList.push(... adminsIds)
            case Ranks.OWNER:
                userList.push(guild.ownerID)
        }

        return userList.find(user => user === memberId) !== undefined

    }

}

const instance = new PermissionsService()
export { instance as PermissionsService }
