import { Tedis } from "tedis"

const tedis = new Tedis({
    host: process.env.REDIS_HOST || "localhost",
    port: 6379
})

export { tedis }