import { Snowflake } from "discord.js";

export interface GroupChannel {
    guildId: string,
    ownerId: Snowflake,
    ownerName: string,
    categoryId: Snowflake,
    voiceId: Snowflake,
    textId: Snowflake,
    number: number,
    lastActivity: number,
}