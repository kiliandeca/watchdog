import { Message } from "discord.js";
import { DynachanAdministration } from "../services/dynachan";
import { CommandHandler } from "./CommandHandler";
import { Ranks } from "../services";

class InstallCommandHandler implements CommandHandler {

    permissionNeeded = Ranks.ADMIN

    async handle(msg: Message): Promise<string> {
        const guild = msg.guild

        if (!guild) { 
            return "Opération impossible en message privé";
        }
    
        await DynachanAdministration.installDynachan(guild)
        return "Done"
    }
    
}

const instance = new InstallCommandHandler()
export { instance as InstallCommandHandler }