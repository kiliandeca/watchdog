import { Message } from "discord.js";
import { CommandHandler } from "./CommandHandler";
import { Ranks } from "../services";

const botVersion = require('../../package.json').version

class VersionCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.USER


    async handle(msg: Message): Promise<string> {
        return `Version: \`${botVersion}\``
    }
    
}

const instance = new VersionCommandHandler()
export { instance as VersionCommandHandler }