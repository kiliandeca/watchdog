import { Message } from "discord.js";
import { CommandHandler } from "./CommandHandler";
import { Ranks } from "../services";
import { DynachanManager } from "../services/dynachan"

class ClearCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.ADMIN

    async handle(msg: Message): Promise<string> {

        const guild = msg.guild

        if (!guild) { 
            return "Opération impossible en message privé";
        }

        await DynachanManager.deleteEveryGroupChannel(guild)

        return "Done"
    }
    
}

const instance = new ClearCommandHandler()
export { instance as ClearCommandHandler }