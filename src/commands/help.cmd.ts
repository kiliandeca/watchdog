import { Message } from "discord.js";
import { CommandHandler } from "./CommandHandler";
import { Ranks } from "../services";

class HelpCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.USER

    async handle(msg: Message): Promise<string> {
        return "```md\n# Liste des commands:\n- help\n- ping\n- version```"
    }
    
}

const instance = new HelpCommandHandler()
export { instance as HelpCommandHandler }