import { Message } from "discord.js";
import { CommandHandler } from "./CommandHandler";
import { DynachanManager } from "../services/dynachan";
import { Ranks } from "../services";

class InfoCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.USER

    async handle(msg: Message): Promise<string> {
        const guild = msg.guild

        if (!guild) { 
            return "Opération impossible en message privé";
        }

        const groupChannels = await DynachanManager.getGroupChannels(guild.id)
        
        const groupChannel = groupChannels.find(groupChannel => groupChannel.textId === msg.channel.id)
        if (!groupChannel) {
            return 'Not a Dynamique channel'
        }

        const guildMember = guild.members.resolve(groupChannel.ownerId)
        if (!guildMember) {
            return 'Can\'t find user'
        }

        return `Owner: ${guildMember.displayName}`

    }
    
}

const instance = new InfoCommandHandler()
export { instance as InfoCommandHandler }