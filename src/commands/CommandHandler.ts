import { Message } from "discord.js";
import { Ranks } from "../services/permissions.service";


export abstract class CommandHandler{
    abstract async handle(msg: Message): Promise<string>;
    
    abstract permissionNeeded: Ranks;
}