import { Message } from "discord.js";
import { CommandHandler } from "./CommandHandler";
import { Ranks } from "../services";

class PingCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.USER

    async handle(msg: Message): Promise<string> {
        return 'pong'
    }
    
}

const instance = new PingCommandHandler()
export { instance as PingCommandHandler }