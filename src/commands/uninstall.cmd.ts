import { Message } from "discord.js";
import { DynachanAdministration } from "../services/dynachan";
import { CommandHandler } from "./CommandHandler";
import { Ranks } from "../services";

class UninstallCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.ADMIN


    async handle(msg: Message): Promise<string> {
        const guild = msg.guild

        if (!guild) { 
            return "Opération impossible en message privé";
        }
    
        await DynachanAdministration.uninstallDynachan(guild)
        return "Done"
    }
    
}

const instance = new UninstallCommandHandler()
export { instance as UninstallCommandHandler }