export enum Commands {
    PING = 'ping',
    HELP = 'help',
    VERSION = 'version',
    INSTALL = 'install',
    UNINSTALL = 'uninstall',
    INFO = 'info',
    PASS_BUTTER = 'pass_butter',
    ADMIN = 'admin',
    CLEAR = 'clear',
}