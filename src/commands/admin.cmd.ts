import { Message, Guild, GuildMember } from "discord.js";
import { CommandHandler } from "./CommandHandler";
import { Ranks, PermissionsService } from "../services";

// To refactor someday, ou peut être pas... unless ?
class AdminCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.ADMIN

    async handle(msg: Message): Promise<string> {

        const guild = msg.guild;

        if (!guild) {
            return "Opération impossible en message privé";
        }

        const args = msg.content.split(' ');
        args.shift() // remove the command

        const subcommand = args.shift()
        if(subcommand === AdminSubCommands.ADD || subcommand === AdminSubCommands.REMOVE) {
            return this.handleSubCommand(guild, args, subcommand);
        }


        const adminsIds = await PermissionsService.getAdminsIds(guild)

        const admins = adminsIds.map(id => guild.members.resolve(id)).filter((value): value is GuildMember => value !== null)

        let response = 
        "```"+
        "Admin list:\n"

        for (const admin of admins) {
            response += `- ${admin.displayName}\n`
        }

        response += "```"

        return response
    }

    async handleSubCommand(guild: Guild, args: string[], subcommand: AdminSubCommands) {

        const memberToSearch = args.shift()

        if (!memberToSearch) {
            return "You need to specify a user"
        }

        // Mention are in the form <@!207492711366918145> so we extract the id
        const memberId = memberToSearch.slice(3, -1)

        switch (subcommand) {
            case AdminSubCommands.ADD:
                await PermissionsService.addAdmin(guild, memberId)
                return `Added <@!${memberId}> to the list of admins`
                break;
            case AdminSubCommands.REMOVE:
                await PermissionsService.removeAdmin(guild, memberId)
                return `Removed <@!${memberId}> from the list of admins`
                break;
        }
        

    }
    
}

enum AdminSubCommands {
    ADD = 'add',
    REMOVE = 'remove',
}

const instance = new AdminCommandHandler()
export { instance as AdminCommandHandler }