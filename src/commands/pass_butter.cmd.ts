import { Message } from "discord.js";
import { CommandHandler } from "./CommandHandler";
import { Ranks } from "../services";

class ButterCommandHandler extends CommandHandler {

    permissionNeeded = Ranks.USER


    async handle(msg: Message): Promise<string> {
        msg.reply(' ', { files:
            [
                "https://media2.giphy.com/media/BCkaO6X2PLG4E/source.gif"
            ]
        })

        return ''
    }
    
}

const instance = new ButterCommandHandler()
export { instance as ButterCommandHandler }